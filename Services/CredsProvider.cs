using System.Text.Json;

namespace PatreonServer;

public sealed class CredsProvider : ICredsProvider
{
    private static readonly string _filePath = "data/patreon_credentials.json";

    public CredsProvider()
    {
        Directory.CreateDirectory("./data");
        if (!File.Exists(_filePath))
        {
            var emptyCreds = JsonSerializer.Serialize(new Creds(), new JsonSerializerOptions { WriteIndented = true });
            File.WriteAllText(_filePath, emptyCreds);
        }
    }

    public Creds GetCreds()
    {
        var json = File.ReadAllText(_filePath);
        return JsonSerializer.Deserialize<Creds>(json)
               ?? throw new InvalidOperationException("Unable to deserialize patreon_credentials.json");
    }

    public void ModifyCredsFile(Action<Creds> action)
    {
        var json = File.ReadAllText(_filePath);
        var creds = JsonSerializer.Deserialize<Creds>(json)!;

        action(creds);

        var jsonNew = JsonSerializer.Serialize(creds, new JsonSerializerOptions { WriteIndented = true });
        File.WriteAllText(_filePath, jsonNew);
    }
}