namespace PatreonServer;

/// <summary>
/// Service tasked with handling pledges on patreon
/// </summary>
public sealed class PatreonSubscriptionHandler
{
    private readonly PatreonClient _patreonClient;
    private readonly ICredsProvider _credsProvider;
    private readonly ILogger<PatreonSubscriptionHandler> _log;

    public PatreonSubscriptionHandler(ICredsProvider credsProvider, ILogger<PatreonSubscriptionHandler> log)
    {
        _credsProvider = credsProvider;
        var creds = _credsProvider.GetCreds();
        _log = log;
        _patreonClient = new PatreonClient(creds.ClientId,
            creds.ClientSecret,
            creds.RefreshToken);
    }
    
    public async IAsyncEnumerable<IReadOnlyCollection<PatreonMemberData>> GetPatronsAsync()
    {
        _log.LogInformation("Getting patrons");
        var botCreds = _credsProvider.GetCreds();

        if (string.IsNullOrWhiteSpace(botCreds.CampaignId)
            || string.IsNullOrWhiteSpace(botCreds.ClientId)
            || string.IsNullOrWhiteSpace(botCreds.ClientSecret)
            || string.IsNullOrWhiteSpace(botCreds.RefreshToken))
        {
            _log.LogWarning("Missing patreon credentials");
            yield break;
        }

        _log.LogInformation("Refreshing token");
        var result = await _patreonClient.RefreshTokenAsync(false);
        if (!result.TryPickT0(out _, out var error))
        {
            _log.LogWarning("Unable to refresh patreon token: {ErrorMessage}", error.Value);
            yield break;
        }

        var patreonCreds = _patreonClient.GetCredentials();

        _credsProvider.ModifyCredsFile(c =>
        {
            c.AccessToken = patreonCreds.AccessToken;
            c.RefreshToken = patreonCreds.RefreshToken;
        });

        IAsyncEnumerable<IEnumerable<PatreonMemberData>> data;
        try
        {
            var maybeUserData = await _patreonClient.GetMembersAsync(botCreds.CampaignId);
            data = maybeUserData.Match(
                static userData => userData,
                err =>
                {
                    _log.LogWarning("Error while getting patreon members: {ErrorMessage}", err.Value);
                    return AsyncEnumerable.Empty<IEnumerable<PatreonMemberData>>();
                });
        }
        catch (Exception ex)
        {
            _log.LogWarning(ex,
                "Unexpected error while refreshing patreon members: {ErroMessage}",
                ex.Message);

            yield break;
        }

        var now = DateTime.UtcNow;

        // this assumes the users will have membership for 31 days since their last payment,
        // not just for the month they're pledging.
        await foreach (var batch in data)
        {
            // send only active patrons
            var toReturn = batch.Where(x => x.EntitledToCents > 0
                                            && x.LastChargeDate is { } lc
                                            && lc.ToUniversalTime() >= now - TimeSpan.FromDays(31))
                .ToArray();

            if (toReturn.Length == 0)
                yield break;
            
            if (toReturn.Length > 0)
                yield return toReturn;
        }
    }
}