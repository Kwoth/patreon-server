namespace PatreonServer;

public interface ICredsProvider
{
    Creds GetCreds();
    void ModifyCredsFile(Action<Creds> action);
}