﻿using Microsoft.EntityFrameworkCore;

namespace PatreonServer;

public class PatreonDataCollector : BackgroundService
{
    private readonly PatreonSubscriptionHandler _subHandler;
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<PatreonDataCollector> _logger;

    public PatreonDataCollector(PatreonSubscriptionHandler subHandler,
        IServiceProvider serviceProvider,
        ILogger<PatreonDataCollector> logger)
    {
        _subHandler = subHandler;
        _serviceProvider = serviceProvider;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await Task.Yield();
        _logger.LogInformation("Starting Patreon data collector");

        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                _logger.LogInformation("Updating Patreon members...");
                var ticks = Environment.TickCount64;
                await foreach (var memberList in _subHandler.GetPatronsAsync().WithCancellation(stoppingToken))
                {
                    if (stoppingToken.IsCancellationRequested)
                        return;

                    using var scope = _serviceProvider.CreateScope();

                    var connectedMembers = memberList.Where(x => x.DiscordUserId is not null).ToArray();

                    var dbContext = scope.ServiceProvider.GetRequiredService<PatreonDbContext>();

                    // await dbContext.Members
                    // .Where(x => x.LastChargeDate < DateTime.UtcNow.AddMonths(-1))
                    // .ExecuteDeleteAsync();

                    foreach (var member in connectedMembers)
                    {
                        // _logger.LogInformation("Updating member {DiscordUserId}", member.DiscordUserId);

                        var existingMember = dbContext.Members
                            .FirstOrDefault(m => m.PatreonUserId == member.PatreonUserId);


                        if (existingMember != null)
                        {
                            // Update existing member
                            existingMember.PatreonUserName = member.PatreonUserName;
                            existingMember.DiscordUserId = member.DiscordUserId!.Value;
                            existingMember.EntitledToCents = member.EntitledToCents;
                            existingMember.LastChargeDate = member.LastChargeDate;
                            existingMember.LastChargeStatus = member.LastChargeStatus;
                        }
                        else
                        {
                            // Add new member
                            dbContext.Members.Add(new PatreonMemberModel()
                            {
                                PatreonUserName = member.PatreonUserName,
                                PatreonUserId = member.PatreonUserId,
                                DiscordUserId = member.DiscordUserId!.Value,
                                EntitledToCents = member.EntitledToCents,
                                LastChargeDate = member.LastChargeDate,
                                LastChargeStatus = member.LastChargeStatus,
                            });
                        }
                    }

                    await dbContext.SaveChangesAsync();
                }

                _logger.LogInformation("Updated Patreon members in {Seconds:F2} seconds!",
                    (Environment.TickCount64 - ticks) / 1000f);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while updating Patreon data");
            }

            await Task.Delay(5000, stoppingToken);
        }
    }
}