﻿#nullable disable
namespace PatreonServer;

public sealed class Creds
{
    public string ClientId { get; set; }
    public string ClientSecret { get; set; }
    public string RefreshToken { get; set; }
    public string AccessToken { get; set; }
    public string CampaignId { get; set; }
}