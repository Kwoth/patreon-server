﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

public class PatreonDbContext : DbContext
{
    public DbSet<PatreonMemberModel> Members { get; set; }
    public DbSet<AccessToken> AccessTokens { get; set; }

    public PatreonDbContext(DbContextOptions<PatreonDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<PatreonMemberModel>()
            .HasIndex(x => x.PatreonUserId)
            .IsUnique();

        modelBuilder.Entity<AccessToken>()
            .HasIndex(x => x.Token)
            .IsUnique();

        modelBuilder.Entity<AccessToken>()
            .HasIndex(x => x.PatreonUserId)
            .IsUnique();
    }
}

public class PatreonMemberModel
{
    [Key]
    public int Id { get; set; }

    [MaxLength(64)]
    public required string PatreonUserId { get; set; }

    public ulong DiscordUserId { get; set; }

    [MaxLength(32)]
    public string? LastChargeStatus { get; set; }

    public int EntitledToCents { get; set; }

    public DateTime? LastChargeDate { get; set; }

    public string PatreonUserName { get; set; } = string.Empty;
}

public sealed class AccessToken
{
    [Key]
    public int Id { get; set; }

    [MaxLength(256)]
    public required string Token { get; init; }

    [MaxLength(64)]
    public required string PatreonUserId { get; init; }
}