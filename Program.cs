using System.Security.Cryptography;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PatreonServer;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddHostedService<PatreonDataCollector>();
builder.Services.AddSingleton<PatreonSubscriptionHandler>();
builder.Services.AddSingleton<ICredsProvider, CredsProvider>();
builder.Services.AddDbContext<PatreonDbContext>(opts =>
{
    opts.UseSqlite(builder.Configuration.GetConnectionString("DefaultConnection"));
});

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var db = scope.ServiceProvider.GetRequiredService<PatreonDbContext>();
    db.Database.Migrate();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapGet("/get-member",
        (HttpContext ctx, [FromServices] PatreonDbContext db, [FromQuery] bool allowExpired = false) =>
        {
            var tokenHeader = ctx.Request.Headers["x-auth-token"];
            var token = tokenHeader.Count == 0 ? null : tokenHeader[0];

            if (token is null)
                return Results.Unauthorized();

            var tokenEntry = db.AccessTokens
                .FirstOrDefault(m => m.Token == token);

            if (tokenEntry is null)
                return Results.Unauthorized();

            var member = db.Members
                .Where(x => (x.PatreonUserId == tokenEntry.PatreonUserId)
                            && (allowExpired || x.LastChargeDate >= DateTime.UtcNow.AddMonths(-1)))
                .FirstOrDefault();

            if (member is null)
                return Results.NotFound();

            return Results.Ok(member);
        })
    .WithOpenApi();


app.MapGet("/get-token",
        async ([FromQuery] string discordUserId, [FromServices] PatreonDbContext db, [FromQuery] bool regen = false) =>
        {
            if (string.IsNullOrWhiteSpace(discordUserId) || !ulong.TryParse(discordUserId, out var uid))
                return Results.BadRequest("Discord user ID is required");

            var member = await db.Members
                .Where(x => x.DiscordUserId == uid && x.LastChargeDate >= DateTime.UtcNow.AddMonths(-1))
                .FirstOrDefaultAsync();

            if (member is null)
                return Results.NotFound();

            var patreonUserId = member.PatreonUserId;

            var existingTokenEntry = await db.AccessTokens
                .FirstOrDefaultAsync(m => m.PatreonUserId == patreonUserId);

            string token = "";
            if (regen || existingTokenEntry is null)
            {
                if (existingTokenEntry != null)
                {
                    db.AccessTokens.Remove(existingTokenEntry);
                    await db.SaveChangesAsync();
                }

                token = GenerateSecureToken();
                var newTokenEntry = new AccessToken { Token = token, PatreonUserId = patreonUserId };
                db.AccessTokens.Add(newTokenEntry);
                await db.SaveChangesAsync();
            }
            else
            {
                token = existingTokenEntry.Token;
            }

            return Results.Ok(token);
        })
    .WithOpenApi();

app.MapGet("/list",
        async ([FromServices] PatreonDbContext db,
            [FromQuery] string? discordUserId,
            [FromQuery] string? userId,
            [FromQuery] string? chargeStatus,
            [FromQuery] string? before,
            [FromQuery] string? after,
            [FromQuery] string? orderBy,
            [FromQuery] string? orderDirection,
            [FromQuery] int? pageNumber,
            [FromQuery] int? pageSize) =>
        {
            var duid = !string.IsNullOrWhiteSpace(discordUserId) && ulong.TryParse(discordUserId, out var parsed)
                ? (ulong?)parsed
                : null;

            var query = db.Members
                .Where(m => (duid == null || m.DiscordUserId == duid))
                .Where(m => (userId == null || m.PatreonUserId == userId))
                .Where(m => (chargeStatus == null || m.LastChargeStatus == chargeStatus));

            if (before != null)
            {
                var beforeDate = DateTime.Parse(before);
                query = query.Where(m => m.LastChargeDate < beforeDate);
            }

            if (after != null)
            {
                var afterDate = DateTime.Parse(after);
                query = query.Where(m => m.LastChargeDate > afterDate);
            }

            query = orderBy switch
            {
                "amount" => orderDirection == "asc"
                    ? query.OrderBy(m => m.EntitledToCents)
                    : query.OrderByDescending(m => m.EntitledToCents),
                "chargeDate" => orderDirection == "asc"
                    ? query.OrderBy(m => m.LastChargeDate)
                    : query.OrderByDescending(m => m.LastChargeDate),
                "chargeStatus" => orderDirection == "asc"
                    ? query.OrderBy(m => m.LastChargeStatus)
                    : query.OrderByDescending(m => m.LastChargeStatus),
                _ => query.OrderByDescending(m => m.LastChargeDate)
            };

            var members = await query
                .Skip(((pageNumber ?? 1) - 1) * (pageSize ?? 10))
                .Take(pageSize ?? 10)
                .ToListAsync();

            return Results.Ok(members);
        })
    .WithOpenApi();

// helper method to generate a secure token
string GenerateSecureToken()
{
    var rng = RandomNumberGenerator.Create();
    Span<byte> bytes = stackalloc byte[64]; // 512-bit token
    rng.GetBytes(bytes);
    return Convert.ToBase64String(bytes);
}

app.Run();