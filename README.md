# Patreon Server

This project is meant to be a sane interface for patreon api.  

It continually loops through the members list on for your specified campaign and puts them in the database.  

The endpoints are unprotected and are meant only to be used via your other services internally in your network.  
ork.

This shouldn't be used yet unless you're happy with very limited amount of data that's stored for each member  


# Setup Instructions

1. `git clone https://gitlab.com/kwoth/patreon-server.git`  
2. `cd patreon-server`  

3. *edit `data/patreon-credentials.json` in your favorite editor. You can find your api creds here https://www.patreon.com/portal*  
*To get campaign id: Go to your patreon creator page, open the inspector, go to console tab and type "window.patreon.bootstrap.campaign.data.id" and press enter*

4. `dotnet run -c Release`

# API

### GET /get-member

Retrieve a member's information.

##### Request Headers

* `X-Auth-Token`: `string` (required) - A valid access token.

##### Response

* `200 OK`: A member object in JSON format.
* `401 Unauthorized`: If the `X-Auth-Token` header is missing or invalid.
* `404 Not Found`: If no member is found associated with the provided token.

##### Example Request

```bash
GET /get-member
X-Auth-Token: valid_access_token_here
```

##### Example Response

```json
{
  "id": 1,
  "discordUserId": 1234567890,
  "patreonUserId": "1234567890",
  "lastChargeDate": "2022-01-01T12:00:00.000Z",
  "lastChargeStatus": "active",
  "entitledToCents": 1000
}
```


### GET /get-token

Retrieve an access token for a Patreon user.

##### Query Parameters

* `discordUserId`: `string` (required) - Discord user ID associated with the Patreon user.

##### Response

* `200 OK`: An existing or newly generated access token in plain text.  
* `400 Bad Request`: If the `discordUserId` query parameter is missing, empty, or not a valid number.  
* `404 Not Found`: If no member is found with the provided `discordUserId`.  

##### Example Request

```bash
GET /get-token?discordUserId=1234567890
```

##### Example Response

```text
existing_or_newly_generated_token_here
```

### GET /regenerate-token

Regenerate an access token for a Patreon user.

##### Query Parameters

 `discordUserId`: `string` (required) - Discord user ID associated with the Patreon user.

##### Response

 `200 OK`: A new access token in plain text.  
 `400 Bad Request`: If the `discordUserId` query parameter is missing or empty.  
 `404 Not Found`: If no member is found with the provided `discordUserId`.  

##### Example Request

``bash
ET /regenerate-token?discordUserId=1234567890
``

##### Example Response

``text
ew_access_token_here
``

### GET /list

Retrieve a list of members with optional filtering and pagination.

##### Query Parameters

* `discordUserId`: `string` (optional) - Filter by Discord user ID.
* `userId`: `string` (optional) - Filter by Patreon user ID.
* `chargeStatus`: `string` (optional) - Filter by charge status.
* `before`: `string` (optional) - Filter by last charge date before a specific date (format: `yyyy-MM-ddTHH:mm:ss.fffZ`).
* `after`: `string` (optional) - Filter by last charge date after a specific date (format: `yyyy-MM-ddTHH:mm:ss.fffZ`).
* `orderBy`: `string` (optional) - Sort by a specific field (`amount`, `chargeDate`, or `chargeStatus`).
* `orderDirection`: `string` (optional) - Sort direction (`asc` or `desc`).
* `pageNumber`: `integer` (optional) - Page number for pagination (default: 1).
* `pageSize`: `integer` (optional) - Page size for pagination (default: 10).

##### Response

* `200 OK`: A list of members in JSON format.

##### Example Request

```bash
GET /list?discordUserId=1234567890&chargeStatus=active&orderBy=amount&orderDirection=desc&pageNumber=2&pageSize=20
```

##### Example Response

```json
[
  {
    "id": 1,
    "discordUserId": 1234567890,
    "patreonUserId": "1234567890",
    "lastChargeDate": "2022-01-01T12:00:00.000Z",
    "lastChargeStatus": "active",
    "entitledToCents": 1000
  },
  {
    "id": 2,
    "discordUserId": 9876543210,
    "patreonUserId": "9876543210",
    "lastChargeDate": "2022-01-15T12:00:00.000Z",
    "lastChargeStatus": "active",
    "entitledToCents": 500
  },
  ...
]
```