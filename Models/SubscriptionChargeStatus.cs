

public enum SubscriptionChargeStatus
{
    Paid,
    Refunded,
    Unpaid,
    Other,
}