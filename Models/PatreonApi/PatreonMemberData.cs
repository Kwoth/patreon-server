﻿#nullable disable
namespace PatreonServer;

public sealed class PatreonMemberData 
{
    public string PatreonUserName { get; init; }
    public string PatreonUserId { get; init; }
    public ulong? DiscordUserId { get; init; }
    public DateTime? LastChargeDate { get; init; }
    public string LastChargeStatus { get; init; }
    public int EntitledToCents { get; init; }

    public SubscriptionChargeStatus ChargeStatus
        => LastChargeStatus switch
        {
            "Paid" => SubscriptionChargeStatus.Paid,
            "Fraud" or "Refunded" => SubscriptionChargeStatus.Refunded,
            "Declined" or "Pending" => SubscriptionChargeStatus.Unpaid,
            _ => SubscriptionChargeStatus.Other,
        };
}